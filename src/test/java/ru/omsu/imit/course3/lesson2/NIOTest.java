package ru.omsu.imit.course3.lesson2;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static org.junit.Assert.*;

public class NIOTest {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private static void  writeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {

        try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file))) {
            osw.write(trainee.getFirstName() + ";");
            osw.write(trainee.getSecondName() + ";");
            osw.write(Integer.toString(trainee.getRating()));
        }
    }

    static private boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    private static Trainee deserializeTrainee(ByteBuffer bb) {
        try (ByteArrayInputStream b = new ByteArrayInputStream(bb.array())) {
            try (ObjectInputStream o = new ObjectInputStream(b)) {
                return (Trainee)o.readObject();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void checkFile(String str) {
        Path p = Paths.get(str);
        Assert.assertTrue(Files.exists(p));
    }

    private static void assertNumbers(Integer[] inputs) {
        for (int i = 0; i < 100; i++) {
            assertEquals(i, (int)inputs[i]);
        }
    }

    private Path createFileWithFiles() throws IOException {
        Path root = Files.createDirectory(folder.getRoot().toPath().resolve("tempDir"));

        Files.createFile(root.resolve("a.bin"));
        Files.createFile(root.resolve("b.txt"));
        Files.createFile(root.resolve("c.bat"));
        Files.createFile(root.resolve("d.exe"));
        Files.createFile(root.resolve("e.raw"));
        Files.createFile(root.resolve("f.dat"));
        Files.createFile(root.resolve("g.dat"));
        Files.createFile(root.resolve("h.dat"));
        Files.createFile(root.resolve("i.dot"));
        Files.createFile(root.resolve("j.bot"));

        return root;
    }

    @Test
    public void testReadTraineeFromByteFile() {
        Trainee trainee = new Trainee("0111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110011111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111100111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111001111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110",
                "abc", 2);
        try {
            File newFile = folder.newFile("test");
            writeTraineeToBinaryFile(newFile, trainee);
            Trainee res = NIO.readByteTrainee(newFile);
            assertEquals(trainee, res);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testReadByteTraineeUsingMappedByteBuffer() {
        Trainee trainee = new Trainee("Achmed", "Edakile", 5);
        try {
            File newFile = folder.newFile("test");
            writeTraineeToBinaryFile(newFile, trainee);
            Trainee res = NIO.readByteTraineeUsingMappedByteBuffer(newFile);
            assertEquals(trainee, res);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testCreateFileWithNumbersUsingMappedByteBuffer() {
        try {
            NIO.createFileWithNumbersUsingMappedByteBuffer("test");
            Integer[] res = NIO.readFileWithNumbers("test");
            assertNumbers(res);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testSerializeTrainee() {
            Trainee trainee = new Trainee("Achmed", "Edakile", 5);
            Trainee res = deserializeTrainee(NIO.serializeTrainee(trainee));
            Assert.assertEquals(trainee, res);
    }

    @Test
    public void testRenameFiles() throws IOException {
        Path p = createFileWithFiles();

        NIO.renameFiles(p);

        Assert.assertEquals(10, Files.list(p).count());

        String[] files = {"a.bin", "b.txt", "c.bat", "d.exe", "e.raw", "f.bin", "g.bin", "h.bin", "i.dot", "j.bot"};
        for (String str: files) {
            checkFile(p.toString() + "/" + str);
        }
        deleteDirectory(new File(p.toString()));
    }

}
