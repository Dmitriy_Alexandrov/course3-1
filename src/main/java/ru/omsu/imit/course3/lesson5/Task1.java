package ru.omsu.imit.course3.lesson5;

import java.util.Arrays;

public class Task1 {


    public static String[] getCurThread(Thread a) {
        return new String[] {a.getName(), a.getThreadGroup().getName(), String.valueOf(a.getId()), String.valueOf(a.getPriority()),
                        String.valueOf(a.getStackTrace().length), a.getState().name(), String.valueOf(a.getClass())};
    }

}

class T1Demo {
    public static void main(String[] args) {
        Thread t = Thread.currentThread();
        String[] res = Task1.getCurThread(t);
        System.out.println(Arrays.toString(res));
    }
}
