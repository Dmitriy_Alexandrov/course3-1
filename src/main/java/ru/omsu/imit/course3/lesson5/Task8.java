package ru.omsu.imit.course3.lesson5;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class SomeObject {
    static public int number = 0;
    static public ReadWriteLock lock = new ReentrantReadWriteLock();
}

class WriteThread implements Runnable {

    public void run() {
        SomeObject.lock.writeLock().lock();
        System.out.println("lock на запись закрыта записывающим потоком " + Thread.currentThread().getId());
        SomeObject.number++;
        System.out.println("Записывающий поток: " + Thread.currentThread().getId() + " записал: " + SomeObject.number);
        System.out.println("lock на запись открыта записывающим потоком " + Thread.currentThread().getId());
        SomeObject.lock.writeLock().unlock();
    }
}

class ReadThread implements Runnable {

    public void run() {
        SomeObject.lock.readLock().lock();
        System.out.println("lock на чтение закрыт читающим потоком " + Thread.currentThread().getId());
        System.out.println("Читающий поток: " + Thread.currentThread().getId() + " прочитал: " + SomeObject.number);
        System.out.println("lock на чтение открыт читающим потоком " + Thread.currentThread().getId());
        SomeObject.lock.readLock().unlock();
    }
}

class T8Demo {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }
    }
}