package ru.omsu.imit.course3.lesson5;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class Task12 <K, V> extends HashMap<K, V> {
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public V get(Object key) {
        lock.readLock().lock();
        System.out.println("Поток " + Thread.currentThread().getName() + " получил элемент индекса " + String.valueOf(key));
        V res = super.get(key);
        lock.readLock().unlock();
        return res;
    }

    @Override
    public V put(K var1, V var2) {
        System.out.println("Поток " + Thread.currentThread().getName() + " пытается записать в " + String.valueOf(var1)
                + " ключ значение " + var2);
        lock.writeLock().lock();
        super.put(var1, var2);
        System.out.println("Поток " + Thread.currentThread().getName() + " записал в " + String.valueOf(var1)
                + " ключ значение " + var2);
        lock.writeLock().unlock();
        return var2;
    }
}

class T12Reader implements Runnable {
    private HashMap<Integer, String> map;

    T12Reader(HashMap<Integer, String> map) {
        this.map = map;
    }

    @Override
    public void run() {
        try {
            System.out.println("Читатель " + Thread.currentThread().getName() + " начал работу");
            for (int i = 0; i < 10; i++) {
                for (Integer j: map.keySet()) {
                    System.out.print(map.get(j) + " ");
                }
                System.out.println();
                Thread.sleep(20);
            }
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        } finally {
            System.out.println("Читатель " + Thread.currentThread().getName() + " закончил работу");
        }
    }
}

class T12Writer implements Runnable {
    private HashMap<Integer, String> map;
    private Integer cnt;

    T12Writer(HashMap<Integer, String> map, Integer cnt) {
        this.map = map;
        this.cnt = cnt;
    }

    @Override
    public void run() {
            try {
                System.out.println("Писатель " + Thread.currentThread().getName() + " начал работу");
                Random rand = new Random();
                for (int i = 0; i < 10; i++) {
                    cnt++;
                    map.put(cnt, String.valueOf(rand.nextInt()));
                    Thread.sleep(50);
                }
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            } finally {
                System.out.println("Писатель " + Thread.currentThread().getName() + " закончил работу");
            }
    }
}

class T12Demo {
    public static void main(String[] args) {
        HashMap<Integer, String> map = new Task12<>();
        Thread thr1 = new Thread(new T12Writer(map, 0), "1");
        Thread thr2 = new Thread(new T12Writer(map, 20), "2");

        Thread thr3 = new Thread(new T12Reader(map), "3");
        Thread thr4 = new Thread(new T12Reader(map), "4");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();

        try {
            thr1.join();
            thr2.join();
            thr3.join();
            thr4.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Main поток закончил работу");
    }
}
