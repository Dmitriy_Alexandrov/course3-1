package ru.omsu.imit.course3.lesson5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class Formatter {
    public String format(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("d-M-y H:m:s.S");
        return sdf.format(date);
    }
}

class DateFormatter implements Runnable {
    private final Formatter formatter;
    private final Date date = new Date();

    DateFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " || " + date.toString() + " || " + formatter.format(date));
        } catch (ParseException e) {
            System.out.println("Произошла ошибка парсинга");
        }
    }
}

class T13Demo {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        Date date = new Date();

        Thread thr1 = new Thread(new DateFormatter(formatter), "1");
        Thread thr2 = new Thread(new DateFormatter(formatter), "2");
        Thread thr3 = new Thread(new DateFormatter(formatter), "3");
        Thread thr4 = new Thread(new DateFormatter(formatter), "4");
        Thread thr5 = new Thread(new DateFormatter(formatter), "5");
        Thread thr6 = new Thread(new DateFormatter(formatter), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        try {
            thr1.join();
            thr2.join();
            thr3.join();
            thr4.join();
            thr5.join();
            thr6.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Main поток закончен.");
    }
}
