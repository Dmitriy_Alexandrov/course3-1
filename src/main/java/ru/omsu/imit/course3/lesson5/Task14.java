package ru.omsu.imit.course3.lesson5;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class Message {
    private String emailAddress = "";
    private String sender = "";
    private String subject = "";
    private String body = "";

    Message() {}

    Message(String emailAddress, String sender, String subject,
                   String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getSender() {
        return sender;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String toString() {
        return "Отправляет " + sender + " на почту " + emailAddress + '\n'
                + "Тема: " + subject + '\n'
                + body;
    }
}

class T14Transport {
    private SMTP server;

    T14Transport(SMTP server) {
        this.server = server;
    }
    public synchronized void send(Message message) {
        server.send(message);
    }
}

class Sender implements Runnable {
    private Message message = new Message();
    private T14Transport transport;

    public Sender(Message message, String email, T14Transport transport) {
        this.message.setEmailAddress(email);
        this.message.setSender(message.getSender());
        this.message.setSubject(message.getSubject());
        this.message.setBody(message.getBody());
        this.transport = transport;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " начинает отправлять письмо");
            transport.send(message);
            Thread.sleep(0);
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        } finally {
            System.out.println(Thread.currentThread().getName() + " закончил отправлять письмо");
        }
    }
}

class SendFromFileService {
    private String emailsFolder;
    private Message message;
    private T14Transport transport;
    private List<Thread> listThreads;

    public SendFromFileService(T14Transport transport, String emailsFolder, Message message) {
        this.emailsFolder = emailsFolder;
        this.message = message;
        this.transport = transport;
        listThreads = new ArrayList<>();
    }

    public void send() {
        int a = 0;
        try(BufferedReader br = new BufferedReader (new FileReader(emailsFolder)))
        {
            String email;
            while ((email = br.readLine()) != null) {
                a++;
                listThreads.add(new Thread(new Sender(message, email, transport), String.valueOf(a)));
            }

            for (Thread thr: listThreads) {
                thr.start();
            }

            for (Thread thr: listThreads) {
                thr.join();
            }

        } catch (IOException e) {
            System.out.println("Либо файл - каталог, либо неможет его создать, либо не может открыть");
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }
    }
}

class SMTP {
    private String folderName;

    SMTP(String folderName) {
        this.folderName = folderName;
    }

    void send(Message message) {

        try(FileWriter writer = new FileWriter(folderName, true))
        {
            writer.write(message.toString() + "\n\n");
            writer.flush();
        }
        catch(IOException ex){
            System.out.println("Либо файл - каталог, либо неможет его создать, либо не может открыть");
        }
    }
}

class T14Demo {
    public static void main(String[] args) {
        Message message = new Message();
        message.setSender("Kang");
        message.setSubject("Lorem Ipsum");
        message.setBody("Таким образом, повышение уровня гражданского сознания играет важную роль в " +
                "формировании соответствующих условий активизации.");

        SMTP server = new SMTP("ret.txt");
        T14Transport transport = new T14Transport(server);

        SendFromFileService sendFromFileService = new SendFromFileService(transport, "emails.txt", message);
        sendFromFileService.send();

        System.out.println("Все письма отправлены");
    }
}
