package ru.omsu.imit.course3.lesson5;

import java.util.ArrayList;

class ArrayOneThread implements Runnable {
    private ArrayList<Integer> array;

    ArrayOneThread(ArrayList<Integer> array) {
        this.array = array;
    }

    @Override
    public void run() {
            System.out.println(Thread.currentThread().getName() + " начала работу");
            for (int i = 0; i < 10; i++) {
                synchronized (array) {
                    int elem = (int) (Math.random() * 10);
                    array.add(elem);
                    System.out.println(Thread.currentThread().getName() + " добавила елемент:" + elem);
                }
                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    System.out.println("Произошло прерывание потока");
                }
            }
            System.out.println(Thread.currentThread().getName() + " закончила работу");
    }
}

class ArrayTwoThread implements Runnable {
    private ArrayList<Integer> array;

    ArrayTwoThread(ArrayList<Integer> array) {
        this.array = array;
    }

    @Override
    public void run() {

            System.out.println(Thread.currentThread().getName() + " начала работу");
            for (int i = 0; i < 10; i++) {
                synchronized (array) {
                    int index = (int) (Math.random() * array.size());
                    if (array.size() > 0) {
                        array.remove((int) index);
                        System.out.println(Thread.currentThread().getName() + " удалила элемент по индексу " + index);
                    } else {
                        System.out.println(Thread.currentThread().getName() + " нечего удалять ");
                    }
                }
                try{
                    Thread.sleep(0);
                } catch(InterruptedException e){
                    System.out.println("Произошло прерывание потока");
                }
            }
            System.out.println(Thread.currentThread().getName() + " Закончила работу");
    }
}

class T4Demo {
    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();
        Thread thr1 = new Thread(new ArrayOneThread(array), "1");
        Thread thr2 = new Thread(new ArrayTwoThread(array), "2");
        Thread thr3 = new Thread(new ArrayOneThread(array), "3");
        Thread thr4 = new Thread(new ArrayTwoThread(array), "4");
        Thread thr5 = new Thread(new ArrayOneThread(array), "5");
        Thread thr6 = new Thread(new ArrayTwoThread(array), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        try {
            thr1.join();
            thr2.join();
            thr3.join();
            thr4.join();
            thr5.join();
            thr6.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Main поток закончен.");
    }
}
