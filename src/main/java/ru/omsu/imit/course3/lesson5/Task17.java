package ru.omsu.imit.course3.lesson5;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;

class Event {
    private int num;
    private String data;

    public Event(String data, int num) {
        this.data = data;
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return getNum() == event.getNum() &&
                Objects.equals(getData(), event.getData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNum(), getData());
    }
}

class T17Task implements Executable {
    protected List<Executable> list;
    private boolean isLast;

    T17Task(boolean isLast) {
        this.isLast = isLast;
        if (!isLast) {
            list = new ArrayList<>();
            Random rand = new Random();
            int length = rand.nextInt(9) + 1;
            for (int i = 0; i < length; i++) {
                if (i % 2 == 0) {
                    list.add(new T16Task(false));
                } else {
                    list.add(new T16Heir(false));
                }
            }
        }
    }

    @Override
    public void execute() {
        //System.out.println("Поток " + Thread.currentThread().getName() + " сейчас решит задачку");
        if (!listIsEmpty()) {
            Executable exec = list.remove(0);
            exec.execute();
        }
    }

    public boolean listIsEmpty() {
        return list.isEmpty();
    }
    public boolean isLast() {
        return isLast;
    }
}

class T17Developer implements Runnable {
    private LinkedBlockingQueue<T17Task> queue;
    private LinkedBlockingQueue<Event> eventQueue;

    T17Developer(LinkedBlockingQueue<T17Task> queue, LinkedBlockingQueue<Event> eventQueue) {
        this.queue = queue;
        this.eventQueue = eventQueue;
    }

    public void run() {
        System.out.println("Разработчик " + Thread.currentThread().getName() + " запущен");
        for (int i = 0; i < 3; i++) {
            T17Task task;
            task = new T17Task(false);
            queue.add(task);
            eventQueue.add(new Event("Add Task", task.list.size()));
            System.out.println("Разработчик " + Thread.currentThread().getName() + " добавил " + i + " многостадийную(" + task.list.size() + ") задачу");
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
        System.out.println("Разработчик " + Thread.currentThread().getName() + " закончил работу");
    }
}

class T17Executor implements Runnable {
    private LinkedBlockingQueue<T17Task> queue;
    private LinkedBlockingQueue<Event> eventQueue;

    T17Executor(LinkedBlockingQueue<T17Task> queue, LinkedBlockingQueue<Event> eventQueue) {
        this.queue = queue;
        this.eventQueue = eventQueue;
    }

    public void run() {
        System.out.println("Исполнитель " + Thread.currentThread().getName() + " запущен");
        while (true) {
            try {
                T17Task task = queue.take();

                if (task.isLast()) {
                    System.out.println("Исполнитель " + Thread.currentThread().getName() + " завершил работу");
                    break;
                }
                System.out.println("Исполнитель " + Thread.currentThread().getName() + " получил T17Task");
                task.execute();
                eventQueue.add(new Event("Consumed Task", 1));
                if (!task.listIsEmpty()) {
                    queue.add(task);
                }
                Thread.sleep(200);
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
    }
}

class T17ExecutorKiller implements Runnable {
    private LinkedBlockingQueue<T17Task> queue;
    private LinkedBlockingQueue<Event> eventQueue;
    private AtomicInteger sumTasks = new AtomicInteger(0);
    private AtomicInteger curTaskNumber = new AtomicInteger(0);
    public boolean producersStopped = false;

    T17ExecutorKiller(LinkedBlockingQueue<T17Task> queue, LinkedBlockingQueue<Event> eventQueue) {
        this.queue = queue;
        this.eventQueue = eventQueue;
    }

    public void addTasks(int task) {
        sumTasks.addAndGet(task);
        //sumTasks += task;
    }

//    public void consumedTask() {
//        curTaskNumber.incrementAndGet();
//        //curTaskNumber++;
//        System.out.println("CURRENT TASK = " + curTaskNumber);
//        if (producersStopped && (sumTasks.get() == curTaskNumber.get())) {
//            kill();
//        }
//    }

    public void getSumTasks() {
        System.out.println("SUM TASKS = " + sumTasks);
    }

    public void getCurTaskNumber() {
        System.out.println("CURRENT TASK NUMBER = " + curTaskNumber);
    }

    private void kill() {
        for (int i = 0; i < 3; i++) {
            queue.add(new T17Task(true));
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Event event = eventQueue.take();
                if (event.getData().equals("Add Task")) {
                    sumTasks.addAndGet(event.getNum());
                } else {
                    producersStopped = true;
                    curTaskNumber.incrementAndGet();
                }
                if (sumTasks.get() == curTaskNumber.get()) {
                    kill();
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class T17Demo {
    public static void main(String[] args) {
        LinkedBlockingQueue<T17Task> queue = new LinkedBlockingQueue<>();
        LinkedBlockingQueue<Event> eventQueue = new LinkedBlockingQueue<>();

        T17ExecutorKiller killer = new T17ExecutorKiller(queue, eventQueue);

        Thread killerThread = new Thread(killer);

        Thread dev1 = new Thread(new T17Developer(queue, eventQueue), "1");
        Thread dev2 = new Thread(new T17Developer(queue, eventQueue), "2");
        Thread dev3 = new Thread(new T17Developer(queue, eventQueue), "3");

        Thread exec1 = new Thread(new T17Executor(queue, eventQueue), "a");
        Thread exec2 = new Thread(new T17Executor(queue, eventQueue), "b");
        Thread exec3 = new Thread(new T17Executor(queue, eventQueue), "c");

        dev1.start();
        dev2.start();
        dev3.start();

        exec1.start();
        exec2.start();
        exec3.start();

        try {
            dev1.join();
            dev2.join();
            dev3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        killerThread.start();

        try {
            exec1.join();
            exec2.join();
            exec3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        killer.getSumTasks();
        killer.getCurTaskNumber();
        System.out.println("Main поток закончил работу");
    }
}

