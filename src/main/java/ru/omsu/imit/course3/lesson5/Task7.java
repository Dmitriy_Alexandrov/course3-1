package ru.omsu.imit.course3.lesson5;

import java.util.concurrent.Semaphore;

class Task7 implements Runnable {
    private Boolean pingPosition;
    private Semaphore semaphore;
    private String[] str = {"Ping 1", "Pong 0"};

    public Task7(Semaphore semaphore, Boolean pingPosition) {
        this.semaphore = semaphore;
        this.pingPosition = pingPosition;
    }

    public void run() {
        if (pingPosition) {
            while (true) {
                synchronized (str) {
                    try {
                        semaphore.acquire();
                    } catch (InterruptedException e) {
                        System.out.println("Произошло прерывание потока");
                    }
                    if (Turn.side) {
                        System.out.println(str[0]);
                        Turn.side = false;
                    }
                    semaphore.release();
                }
            }
        } else {
            while (true) {
                synchronized (str) {
                    try {
                        semaphore.acquire();
                    } catch (InterruptedException e) {
                        System.out.println("Произошло прерывание потока");
                    }
                    if (!Turn.side) {
                        System.out.println(str[1]);
                        Turn.side = true;
                    }
                    semaphore.release();
                }
            }
        }
    }
}

class Turn {
    public static boolean side = true;
}

class T7Demo {
    public static void main(String[] args) {
        Semaphore  semaphore = new Semaphore(1);
        Thread thr1 = new Thread( new Task7(semaphore, true), "1");
        Thread thr2 = new Thread( new Task7(semaphore, false), "2");
        thr1.start();
        thr2.start();

        try {
            thr1.join();
            thr2.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Конец работы главного потока задачи 7");
    }
}
