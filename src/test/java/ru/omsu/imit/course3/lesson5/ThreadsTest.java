package ru.omsu.imit.course3.lesson5;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.fail;


public class ThreadsTest {

    @Test
    public void testShowThread() { // 1
        Thread t = Thread.currentThread();
        String[] res = Task1.getCurThread(t);
        Assert.assertEquals("main", res[0]);
        Assert.assertEquals("main", res[1]);
        Assert.assertEquals("1", res[2]);
        Assert.assertEquals("5", res[3]);
        Assert.assertEquals("25", res[4]);
        Assert.assertEquals("RUNNABLE", res[5]);
        Assert.assertEquals("class java.lang.Thread", res[6]);
    }

    @Test
    public void testCreateThreadWaitRunning() throws InterruptedException { // 2
        Task2 nt = new Task2();

        nt.getThread().join();

        for (int i = 5; i > 0; i--) {
            System.out.println("Main поток: " + i);
            Thread.sleep(1000);
        }

        System.out.println("Main поток закончил работу");
    }

    @Test
    public void testCreate3ThreadsWaitRunning() throws InterruptedException { // 3
        Onethread nt1 = new Onethread();
        Twothread nt2 = new Twothread();
        Threethread nt3 = new Threethread();

        nt1.getThread().join();
        nt2.getThread().join();
        nt3.getThread().join();

        for (int i = 5; i > 0; i--) {
            System.out.println("Main поток: " + i);
            Thread.sleep(1000);
        }

        System.out.println("Main поток завершил работу");
    }

    @Test
    public void testCreateSynchronisedThreads() throws InterruptedException { // 4
        ArrayList<Integer> array = new ArrayList<>();
        Thread thr1 = new Thread(new ArrayOneThread(array), "1");
        Thread thr2 = new Thread(new ArrayTwoThread(array), "2");
        Thread thr3 = new Thread(new ArrayOneThread(array), "3");
        Thread thr4 = new Thread(new ArrayTwoThread(array), "4");
        Thread thr5 = new Thread(new ArrayOneThread(array), "5");
        Thread thr6 = new Thread(new ArrayTwoThread(array), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();
        thr5.join();
        thr6.join();


        System.out.println("Main поток закончен.");
    }

    @Test
    public void testSyncronizedClasses() throws InterruptedException { // 5
        ArrayServiceT5 as = new ArrayServiceT5(new ArrayList<>());
        Thread thr1 = new Thread(new Task5(as, true), "1");
        Thread thr2 = new Thread(new Task5(as, false), "2");
        Thread thr3 = new Thread(new Task5(as, true), "3");
        Thread thr4 = new Thread(new Task5(as, false), "4");
        Thread thr5 = new Thread(new Task5(as, true), "5");
        Thread thr6 = new Thread(new Task5(as, false), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();
        thr5.join();
        thr6.join();


        System.out.println("Конец работы главного потока задачи 5");
    }

    @Test
    public void testSynchronisedList() throws InterruptedException { // 6
        List list = Collections.synchronizedList(new ArrayList<Integer>());
        //List list = new ArrayList<Integer>();
        ArrayServiceT6 as = new ArrayServiceT6(list);

        Thread thr1 = new Thread( new Task6(as, true), "1");
        Thread thr2 = new Thread( new Task6(as, false), "2");
        Thread thr3 = new Thread( new Task6(as, true), "3");
        Thread thr4 = new Thread( new Task6(as, false), "4");
        Thread thr5 = new Thread( new Task6(as, true), "5");
        Thread thr6 = new Thread( new Task6(as, false), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();
        thr5.join();
        thr6.join();

        System.out.println("Конец работы главного потока задачи 6");
    }


    @Test
    public void testPingPong() throws InterruptedException { // 7
        Semaphore  semaphore = new Semaphore(1);
        Thread thr1 = new Thread( new Task7(semaphore, true), "1");
        Thread thr2 = new Thread( new Task7(semaphore, false), "2");
        thr1.start();
        thr2.start();

        thr1.join();
        thr2.join();


        System.out.println("Конец работы главного потока задачи 7");
    }

    @Test
    public void testReadWrite() throws InterruptedException { // 8
        for (int i = 0; i < 3; i++) {
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
            new Thread(new ReadThread()).start();
            new Thread(new WriteThread()).start();
        }
        Thread.sleep(5000);
    }

    @Test
    public void testSynchroniseThreadsReentrantLock() throws InterruptedException { // 10
        ArrayList<Integer> array = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        Thread thr1 = new Thread(new T10ArrayOneThread(lock, array), "1");
        Thread thr2 = new Thread(new T10ArrayOneThread(lock, array), "2");
        Thread thr3 = new Thread(new T10ArrayOneThread(lock, array), "3");
        Thread thr4 = new Thread(new T10ArrayOneThread(lock, array), "4");
        Thread thr5 = new Thread(new T10ArrayOneThread(lock, array), "5");
        Thread thr6 = new Thread(new T10ArrayOneThread(lock, array), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();
        thr5.join();
        thr6.join();


        System.out.println("Main поток закончен.");
    }

    @Test
    public void testPingPongReentrantLock() throws InterruptedException { // 11
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Thread thr1 = new Thread( new Task11(lock, condition, true), "1");
        Thread thr2 = new Thread( new Task11(lock, condition, false), "2");
        thr1.start();
        thr2.start();

        thr1.join();
        thr2.join();

        System.out.println("Конец работы главного потока задачи 7");
    }

    @Test
    public void testNonVolatileConcurrentHashMap() throws InterruptedException { // 12
        HashMap<Integer, String> map = new Task12<>();
        Thread thr1 = new Thread(new T12Writer(map, 0), "1");
        Thread thr2 = new Thread(new T12Writer(map, 20), "2");

        Thread thr3 = new Thread(new T12Reader(map), "3");
        Thread thr4 = new Thread(new T12Reader(map), "4");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();

        thr1.join();
        thr2.join();
        thr3.join();
        thr4.join();

        System.out.println("Main поток закончил работу");
    }

}
