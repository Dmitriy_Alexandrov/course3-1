package ru.omsu.imit.course3.lesson5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Task6 implements Runnable {
    private ArrayServiceT6 as;
    private Boolean addRandom; // true - добавлять в array, false - удалять из array

    Task6(ArrayServiceT6 as, Boolean addRandom) {
        this.as = as;
        this.addRandom = addRandom;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " начала работу");
        if (addRandom) {
            for (int i = 0; i < 10; i++) {
                int elem = as.addRandom();
                System.out.println(Thread.currentThread().getName() + " добавила элемент: " + elem);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("Произошло прерывание потока");
                }
            }
        } else {
            for (int i = 0; i < 10; i++) {
                int elem = as.delRandom();

                if (elem >= 0) {
                    System.out.println(Thread.currentThread().getName() + " удалила элемент по индексу: " + elem);
                } else {
                    System.out.println(Thread.currentThread().getName() + " не нашла элемент по индексу: " + elem * (-1));
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    System.out.println("Произошло прерывание потока");
                }
            }
        }
        System.out.println(Thread.currentThread().getName() + " Закончила работу");
    }
}

class ArrayServiceT6 {
    private List al;
    private int elem;

    ArrayServiceT6(List al) {
        this.al = al;
    }

    public synchronized int addRandom()  {
            elem = (int)(Math.random()*10);
            al.add(elem);
            return elem;
    }

    public synchronized int delRandom() {
            elem = (int)(Math.random()*10);
            try {
                al.remove((int)elem);
            } catch (IndexOutOfBoundsException e) {
                return elem*(-1);
            }
            return elem;
    }
}

class T6Demo {
    public static void main(String[] args) {
        List list = Collections.synchronizedList(new ArrayList<Integer>());
        ArrayServiceT6 as = new ArrayServiceT6(list);

        Thread thr1 = new Thread( new Task6(as, true), "1");
        Thread thr2 = new Thread( new Task6(as, false), "2");
        Thread thr3 = new Thread( new Task6(as, true), "3");
        Thread thr4 = new Thread( new Task6(as, false), "4");
        Thread thr5 = new Thread( new Task6(as, true), "5");
        Thread thr6 = new Thread( new Task6(as, false), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        try {
            thr1.join();
            thr2.join();
            thr3.join();
            thr4.join();
            thr5.join();
            thr6.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Конец работы главного потока задачи 6");
    }
}
