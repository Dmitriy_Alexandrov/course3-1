package ru.omsu.imit.course3.lesson5;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

class T15Data {
    private int[] arr;
    private boolean isLast;

    T15Data(boolean isLast) {
        this.isLast = isLast;
        if (!isLast) {
            Random rand = new Random();
            arr = new int[rand.nextInt(9) + 1];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = rand.nextInt(10);
            }
        }
    }
    public int[] get() {
        return arr;
    }

    public boolean isLast() {
        return isLast;
    }
}

class T15Producer implements Runnable {

    private LinkedBlockingQueue<T15Data> queue;

    T15Producer(LinkedBlockingQueue<T15Data> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Производитель " + Thread.currentThread().getName() + " запущен");
        for (int i = 0; i < 5; i++) {
            queue.add(new T15Data(false));

            System.out.println("Производитель " + Thread.currentThread().getName() + " добавил " + i + " дату");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
        System.out.println("Производитель " + Thread.currentThread().getName() + " закончил работу");
    }
}

class T15Consumer implements Runnable {

    private LinkedBlockingQueue<T15Data> queue;

    public T15Consumer(LinkedBlockingQueue<T15Data> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Потребитель " + Thread.currentThread().getName() + " запущен");
        while (true) {
            try {
                T15Data data = queue.take();
                if (data.isLast()) {
                    System.out.println("Потребитель " + Thread.currentThread().getName() + " закончил свою работу");
                    break;
                }
                System.out.println("Потребитель " + Thread.currentThread().getName() + " получил " + Arrays.toString(data.get()));
                Thread.sleep(50);
            }
             catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
    }
}

class T15Demo {
    public static void main(String[] args) {
        LinkedBlockingQueue<T15Data> queue = new LinkedBlockingQueue<>();

        Thread prod1 = new Thread(new T15Producer(queue), "1");
        Thread prod2 = new Thread(new T15Producer(queue), "2");
        Thread prod3 = new Thread(new T15Producer(queue), "3");

        Thread cons3 = new Thread(new T15Consumer(queue), "c");
        Thread cons2 = new Thread(new T15Consumer(queue), "b");
        Thread cons1 = new Thread(new T15Consumer(queue), "a");

        prod1.start();
        prod2.start();
        prod3.start();

        cons1.start();
        cons2.start();
        cons3.start();

        try {
            prod1.join();
            prod2.join();
            prod3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }


        for (int i = 0; i < 3; i++) {
            queue.add(new T15Data(true));
        }

        try {
            cons1.join();
            cons2.join();
            cons3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }


        System.out.println("Main поток закончил работу");
    }
}
