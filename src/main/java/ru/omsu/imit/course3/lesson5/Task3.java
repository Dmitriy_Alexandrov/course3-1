package ru.omsu.imit.course3.lesson5;

class Onethread extends Task2 {
    @Override
    public void run()
    {
        System.out.println("Onethread начала работу");
        try {
            for (int i = 15; i > 0; i--) {
                System.out.println("Onethread работает: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Onethread прервался.");
        }
        System.out.println("Onethread Закончила работу");
    }
}

class Twothread extends Task2 {
    @Override
    public void run()
    {
        System.out.println("Twothread начала работу");
        try {
            for (int i = 10; i > 0; i--) {
                System.out.println("Twothread работает: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Twothread прервался.");
        }
        System.out.println("Twothread Закончила работу");
    }
}

class Threethread extends Task2 {
    @Override
    public void run()
    {
        System.out.println("Threethread начала работу");
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Threethread работает: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Threethread прервался.");
        }
        System.out.println("Threethread Закончила работу");
    }
}

class T3Demo {
    public static void main(String[] args) {
        Onethread nt1 = new Onethread();
        Twothread nt2 = new Twothread();
        Threethread nt3 = new Threethread();

        try {
            nt1.getThread().join();
            nt2.getThread().join();
            nt3.getThread().join();

            for (int i = 5; i > 0; i--) {
                System.out.println("Main поток: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Main поток завершил работу");
    }
}
