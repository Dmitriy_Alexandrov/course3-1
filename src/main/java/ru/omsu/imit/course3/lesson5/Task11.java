package ru.omsu.imit.course3.lesson5;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class Task11 implements Runnable {
    private Boolean pingPosition;
    private ReentrantLock lock;
    private Condition condition;
    private String[] str = {"Ping 11", "Pong 0"};

    public Task11(ReentrantLock lock, Condition condition, Boolean pingPosition) {
        this.lock = lock;
        this.condition = condition;
        this.pingPosition = pingPosition;
    }

    public void run() {
        if (pingPosition) {
            while (true) {
                lock.lock();
                try {
                    while (!T11Turn.side)
                        condition.await();
                    System.out.println(str[0]);
                    T11Turn.side = false;
                    condition.signal();
                } catch (InterruptedException e) {
                    System.out.println("Произошло прерывание потока");
                } finally {
                    lock.unlock();
                }
            }
        } else {
            while (true) {
                lock.lock();
                try {
                    while (T11Turn.side)
                        condition.await();
                    System.out.println(str[1]);
                    T11Turn.side = true;
                    condition.signal();
                } catch (InterruptedException e) {
                    System.out.println("Произошло прерывание потока");
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}

class T11Turn {
    public static boolean side = true;
}

class T11Demo {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Thread thr1 = new Thread( new Task11(lock, condition, true), "1");
        Thread thr2 = new Thread( new Task11(lock, condition, false), "2");
        thr1.start();
        thr2.start();

        try {
            thr1.join();
            thr2.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Конец работы главного потока задачи 7");
    }
}
