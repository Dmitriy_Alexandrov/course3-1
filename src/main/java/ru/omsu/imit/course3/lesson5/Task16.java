package ru.omsu.imit.course3.lesson5;

import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

interface Executable {
    void execute();
}

class T16Task implements Executable {
    protected int[] arr;
    private boolean isLast;

    T16Task(boolean isLast) {
        this.isLast = isLast;
        if (!isLast) {
            Random rand = new Random();
            arr = new int[rand.nextInt(9) + 1];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = rand.nextInt(10);
            }
        }
    }

    @Override
    public void execute() {
        int sum = 0;
        for (int elem: arr) {
            sum += elem;
        }
        System.out.println("Сумма = " + sum);
    }

    public boolean isLast() {
        return isLast;
    }
}

class T16Heir extends T16Task {

    T16Heir(boolean isLast) {super(isLast);}

    @Override
    public void execute() {
        int sum = 1;
        for (int elem: arr) {
            sum *= elem;
        }
        System.out.println("Произведение = " + sum);
    }
}

class T16Developer implements Runnable {

    private LinkedBlockingQueue<T16Task> queue;

    public T16Developer(LinkedBlockingQueue<T16Task> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Разработчик " + Thread.currentThread().getName() + " запущен");
        for (int i =0; i < 5; i++) {
            if (i % 2 == 0) {
                queue.add(new T16Heir(false));
                System.out.println("Разработчик " + Thread.currentThread().getName() + " добавил " + i + " задачу на произведение");
            } else {
                queue.add(new T16Task(false));
                System.out.println("Разработчик " + Thread.currentThread().getName() + " добавил " + i + " задачу на сумму");
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
        System.out.println("Разработчик " + Thread.currentThread().getName() + " закончил работу");
    }
}

class T16Executor implements Runnable {

    private LinkedBlockingQueue<T16Task> queue;

    T16Executor(LinkedBlockingQueue<T16Task> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Исполнитель " + Thread.currentThread().getName() + " запущен");
        while (true) {
            try {
                T16Task task = queue.take();
                if (task.isLast()) {
                    System.out.println("Исполнитель " + Thread.currentThread().getName() + " закончил свою работу");
                    break;
                }
                System.out.println("Исполнитель " + Thread.currentThread().getName() + " получил T16Task");
                task.execute();
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Произошло прерывание потока");
            }
        }
    }
}

class T16Demo {
    public static void main(String[] args) {
        LinkedBlockingQueue<T16Task> queue = new LinkedBlockingQueue<>();

        Thread dev1 = new Thread(new T16Developer(queue), "1");
        Thread dev2 = new Thread(new T16Developer(queue), "2");
        Thread dev3 = new Thread(new T16Developer(queue), "3");

        Thread exec1 = new Thread(new T16Executor(queue), "a");
        Thread exec2 = new Thread(new T16Executor(queue), "b");
        Thread exec3 = new Thread(new T16Executor(queue), "c");

        dev1.start();
        dev2.start();
        dev3.start();

        exec1.start();
        exec2.start();
        exec3.start();

        try {
            dev1.join();
            dev2.join();
            dev3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        for (int i = 0; i < 3; i++) {
            queue.add(new T16Task(true));
        }

        try {
            exec1.join();
            exec2.join();
            exec3.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }
        System.out.println("Main поток закончил работу");
    }
}
