package ru.omsu.imit.course3.lesson2;

import java.io.Serializable;
import java.util.Objects;

public class Trainee implements Serializable {
    private String firstName;
    private String secondName;
    private int rating;

    Trainee(String firstName, String secondName, int rating) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "firstName = " + firstName +
                ", secondName = " + secondName +
                ", rating = " + rating;
    }

    String getFirstName() {
        return firstName;
    }

    String getSecondName() {
        return secondName;
    }

    int getRating() {
        return rating;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainee trainee = (Trainee) o;
        return rating == trainee.rating &&
                firstName.equals(trainee.firstName) &&
                secondName.equals(trainee.secondName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, rating);
    }
}
