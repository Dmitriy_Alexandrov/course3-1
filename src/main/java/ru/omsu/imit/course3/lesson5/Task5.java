package ru.omsu.imit.course3.lesson5;

import java.util.ArrayList;

class Task5 implements Runnable {
    private ArrayServiceT5 as;
    private Boolean addRandom; // true - добавлять в array, false - удалять из array

    Task5(ArrayServiceT5 as, Boolean addRandom) {
        this.as = as;
        this.addRandom = addRandom;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " начала работу");
        if (addRandom) {
            for (int i = 0; i < 10; i++) {
                int elem = as.addRandom();
                System.out.println(Thread.currentThread().getName() + " добавила элемент: " + elem);
                try{
                    Thread.sleep(0);
                } catch(InterruptedException e){
                    System.out.println("Произошло прерывание потока");
                }
            }
        } else {
            for (int i = 0; i < 10; i++) {
                int elem = as.delRandom();
                if (elem >= 0) {
                    System.out.println(Thread.currentThread().getName() + " удалила элемент по индексу: " + elem);
                } else {
                    System.out.println(Thread.currentThread().getName() + " не нашла элемент по индексу: " + elem * (-1));
                }
                try{
                    Thread.sleep(0);
                } catch(InterruptedException e){
                    System.out.println("Произошло прерывание потока");
                }
            }
        }
        System.out.println(Thread.currentThread().getName() + " Закончила работу");
    }
}

    // Класс с синхронизированными методами

class ArrayServiceT5 {
    private ArrayList<Integer> list;
    private int elem;

    ArrayServiceT5(ArrayList<Integer> al) {
        this.list = al;
    }

    public synchronized int addRandom() {
        elem = (int)(Math.random()*10);
        list.add(elem);
        return elem;
    }

    public synchronized int delRandom() {
        elem = (int)(Math.random()*10);
        try {
            list.remove((int)elem);
        } catch (IndexOutOfBoundsException e) {
            return elem*(-1);
        }
        return elem;
    }
}

class T5Demo {
    public static void main(String[] args) {
        ArrayServiceT5 as = new ArrayServiceT5(new ArrayList<>());
        Thread thr1 = new Thread(new Task5(as, true), "1");
        Thread thr2 = new Thread(new Task5(as, false), "2");
        Thread thr3 = new Thread(new Task5(as, true), "3");
        Thread thr4 = new Thread(new Task5(as, false), "4");
        Thread thr5 = new Thread(new Task5(as, true), "5");
        Thread thr6 = new Thread(new Task5(as, false), "6");
        thr1.start();
        thr2.start();
        thr3.start();
        thr4.start();
        thr5.start();
        thr6.start();

        try {
            thr1.join();
            thr2.join();
            thr3.join();
            thr4.join();
            thr5.join();
            thr6.join();
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Конец работы главного потока задачи 5");
    }
}

