package ru.omsu.imit.course3.lesson4;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

public class Java8UtilsTest {

    @Test
    public void splitAndCount() { // 4/a
        assertEquals(java.util.Optional.of(3), java.util.Optional.of(Java8Utils.splitAndCount.apply("A B C")));
    }

    @Test
    public void splitAndCount2() { // 4/b
        assertEquals(java.util.Optional.of(4), java.util.Optional.of(Java8Utils.splitAndCount2.apply("123 AB CDEF FKB")));
    }

    @Test
    public void TestGetMothersMotherFatherL2() { // 12/a
        // есть отец бабушки
        PersonL2 expected = new PersonL2(null, null);
        PersonL2 person = new PersonL2(new PersonL2(new PersonL2(null, expected), null), null);
        assertEquals(expected, person.getMothersMotherFather());

        // без отца
        person = new PersonL2(new PersonL2(new PersonL2(null, null),null),null);
        assertNull(person.getMothersMotherFather());

        // без бабушки
        person = new PersonL2(new PersonL2(null, null), null);
        assertNull(person.getMothersMotherFather());

        // без матери
        person = new PersonL2(null, null);
        assertNull(person.getMothersMotherFather());
    }

    @Test
    public void TestMothersMotherFatherL3() { // 12/b
        // все есть
        PersonL3 expected = new PersonL3(null, null);
        PersonL3 person = new PersonL3(new PersonL3(new PersonL3(null, expected), null), null);
        assertEquals(expected, person.getMothersMotherFather());

        // без отца
        person = new PersonL3(new PersonL3(new PersonL3( null, null), null), null);
        assertNull(person.getMothersMotherFather());

        // без бабушки
        person = new PersonL3(new PersonL3(null, null), null);
        assertNull(person.getMothersMotherFather());

        // без матери
        person = new PersonL3(null, null);
        assertNull(person.getMothersMotherFather());
    }

    @Test
    public void TestTransform() { // 13
        int[] result = Java8Utils.transform(
                Arrays.stream(new int[]{1, 2, 3, 4, 5}),
                (x) -> x * x
        ).toArray();

        System.out.println(Arrays.toString(result));
        assertArrayEquals(new int[]{1, 4, 9, 16, 25}, result);
    }

    @Test
    public void transformParallel() { // 14
        int[] result = Java8Utils.transform(
                Arrays.stream(new int[]{1, 2, 3, 4, 5}),
                (x) -> x * x
        ).toArray();

        System.out.println(Arrays.toString(result));
        assertArrayEquals(new int[]{1, 4, 9, 16, 25}, result);
    }

    @Test
    public void TestSortPersonsNameLength() { // 15
        List<Person> list = Arrays.asList(
                new Person("Kolya", 20),
                new Person("Bob", 35),
                new Person("Arsen", 31),
                new Person("Nikita", 41),
                new Person("Andrey", 38),
                new Person("Artyom", 40),
                new Person("Stas", 44)
        );
        String[] expected = new String[]{"Bob", "Stas", "Arsen", "Nikita", "Andrey", "Artyom"};

        assertArrayEquals(expected, Java8Utils.sortPersonsNameLength(list));
    }

    @Test
    public void TestSortPersonsEqualName() { // 16
        List<Person> list = Arrays.asList(
                new Person("Kolya", 20),
                new Person("Kolya", 35),
                new Person("Arsen", 31),
                new Person("Arsen", 41),
                new Person("Andrey", 38),
                new Person("Stas", 40),
                new Person("Andrey", 44)
        );

        String[] expected = new String[]{"Stas", "Kolya", "Arsen", "Andrey"};

        assertArrayEquals(expected, Java8Utils.sortPersonsEqualName(list));
    }

    @Test
    public void sum() {
        assertEquals(15, Java8Utils.sum(Arrays.asList(0, 1, 2, 3, 4, 5)));
    }

    @Test
    public void product() {
        assertEquals(-36, Java8Utils.product(Arrays.asList(-1, 1, 2, -2, 3, -3)));
    }

}