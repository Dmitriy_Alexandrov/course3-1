package ru.omsu.imit.course3.lesson5;

public class Task2 extends Thread {
    private Thread t;

    Task2() {
        t = new Thread(this, "Task2Thread");
        t.start();
    }

    Task2(String name) {
        t = new Thread(this, name);
        t.start();
    }

    public Thread getThread() {
        return t;
    }

    public void setThread(Thread t) {
        this.t = t;
    }

    @Override
    public void run()
    {
        System.out.println("Task2Thread начала работу");
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Task2Thread работает: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Task2Thread прервался.");
        }
        System.out.println("Task2Thread Закончила работу");
    }
}

class T2Demo {
    public static void main(String[] args) {
        Task2 nt = new Task2();

        try {
            nt.getThread().join();

            for (int i = 5; i > 0; i--) {
                System.out.println("Main поток: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Произошло прерывание потока");
        }

        System.out.println("Main поток закончил работу");
    }
}
