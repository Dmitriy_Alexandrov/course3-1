package ru.omsu.imit.course3.lesson4;

public class PersonL2 { // 12/1
    private PersonL2 mother;
    private PersonL2 father;

    public PersonL2(PersonL2 mother, PersonL2 father) {
        this.mother = mother;
        this.father = father;
    }

    public PersonL2 getMother() {
        return mother;
    }

    public PersonL2 getFather() {
        return father;
    }

    public PersonL2 getMothersMotherFather() {
        if (mother != null && mother.mother != null) {
            return mother.mother.father;
        }
        return null;
    }
}
