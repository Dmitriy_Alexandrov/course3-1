package ru.omsu.imit.course3.lesson4;

import java.util.Optional;


public class PersonL3 { // 12/2
    private Optional<PersonL3> mother;
    private Optional<PersonL3> father;

    public PersonL3(PersonL3 mother, PersonL3 father) {
        this.mother = Optional.ofNullable(mother);
        this.father = Optional.ofNullable(father);
    }

    public PersonL3 getMother() {
        return mother.orElse(null);
    }

    public PersonL3 getFather() {
        return father.orElse(null);
    }

    public PersonL3 getMothersMotherFather() {
        return mother.flatMap(p -> p.mother).flatMap(p -> p.father).orElse(null);
    }
}
