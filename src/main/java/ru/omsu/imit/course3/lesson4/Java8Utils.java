package ru.omsu.imit.course3.lesson4;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Java8Utils {
    public static Function<String, List<String>> split = str -> Arrays.asList(str.split(" "));

    public static Function<List<?>, Integer> count = List::size;

    public static Function<String, Integer> splitAndCount = split.andThen(count); // 4/a
    public static Function<String, Integer> splitAndCount2 = count.compose(split); // 4/b

    public static MyFunction<String, Person> create = Person::new; // 5

    public static BinaryOperator<Double> max = Math::max; // 6

    public static Supplier<Date> getCurrentDate = Date::new; // 7

    public static Predicate<Integer> isEven = x -> x % 2 == 0; // 8

    public static BiPredicate<Integer, Integer> areEqual = Integer::equals; // 9

    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    } // 13

    public static IntStream transformParallel(IntStream stream, IntUnaryOperator op) { // 14
        return stream.parallel().map(op);
    }

    public static String[] sortPersonsNameLength(List<Person> list) { // 15
        return list.stream()
                .filter(person -> person.getAge() > 30)
                .map(Person::getName)
                .distinct()
                .sorted(Comparator.comparing(String::length))
                .toArray(String[]::new);
    }

    public static String[] sortPersonsEqualName(List<Person> list) { // 16
        return list.stream()
                .filter(person -> person.getAge() > 30)
                .map(Person::getName)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .toArray(String[]::new);
    }

    public static int sum(List<Integer> list) { // 17
        return list.stream()
                .reduce(0, Integer::sum);
    }

    public static int product(List<Integer> list) { // 17
        return list.stream()
                .reduce(1, (prev, cur) -> prev * cur);
    }
}
