package ru.omsu.imit.course3.lesson4;

import java.util.function.Function;

@FunctionalInterface
public interface MyFunction<T, K> { // 10
    K apply(T arg);
//    K apply(T arg1, T arg2);
}
