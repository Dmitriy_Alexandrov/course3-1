package ru.omsu.imit.course3.lesson2;

import java.nio.ByteBuffer;
import java.io.*;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.*;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
class NIO {

    static Trainee readByteTrainee(File file) throws IOException {
        try (FileChannel fc = FileChannel.open(file.toPath(),
                StandardOpenOption.READ)) {
            if (fc.size() > 0) {
                ByteBuffer buffer = ByteBuffer.allocate((int) fc.size());
                fc.read(buffer);
                String[] strArr = new String(buffer.array(), StandardCharsets.UTF_8).split(";");
                return new Trainee(strArr[0], strArr[1], Integer.valueOf(strArr[2]));
            }
        }
        return null;
    }

    static Trainee readByteTraineeUsingMappedByteBuffer(File file) {
        try (FileChannel channel = FileChannel.open(file.toPath(), StandardOpenOption.READ,
                StandardOpenOption.WRITE))
        {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            CharBuffer bufferCopy = Charset.forName("UTF-8").decode(buffer.asReadOnlyBuffer());
            String res = new String(bufferCopy.toString());
            String[] strArr = res.split(";");
            return new Trainee(strArr[0], strArr[1], Integer.valueOf(strArr[2]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return null;
    }

    static void createFileWithNumbersUsingMappedByteBuffer(String file) throws IOException {
        MappedByteBuffer mbb = new RandomAccessFile(file, "rw").getChannel().map(
                FileChannel.MapMode.READ_WRITE, 0, 400);
        for (int i = 0; i < 100; i++) {
            mbb.putInt(i);
        }
    }

    static Integer[] readFileWithNumbers(String file) throws IOException {
        List<Integer> myList = new ArrayList<>();
        try(DataInputStream fis = new DataInputStream(new FileInputStream(new File(file)))) {
            for (int i = 0; i < 100; i++) {
                myList.add(fis.readInt());
            }
        }
        return myList.toArray(new Integer[myList.size()]);
    }

    static ByteBuffer serializeTrainee(Trainee trainee) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
                oos.writeObject(trainee);
            }
            return ByteBuffer.wrap(baos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    static void renameFiles(Path path) {
        File[] file = new File (path.toString()).listFiles();
        if (file != null) {
            for (File f: file) {
                if (f.getName().endsWith(".dat")) {
                    String oldpath = f.getPath();

                    int index = f.getPath().lastIndexOf('.');
                    StringBuilder newpath = new StringBuilder();
                    for (int i = 0; i < index; i++) {
                        newpath.append(oldpath.charAt(i));
                    }
                    newpath.append(".bin");
                    f.renameTo(new File(newpath.toString()));
                }
            }
        }
    }
}